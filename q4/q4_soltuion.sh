#!/bin/bash
set -o nounset
set -o errexit
	
# Question 4: shell script text maniupation 
# This script will take in a .gct file as input and produce 
# the required outputs in the current directory. 

if [ "$#" -ne 1 ]; then
    echo "USAGE: bash q4_solution.sh gct_file"
    echo "Specify the .gct input file. Output files will be created in the current directory."
    exit 1;
fi

# q4.1: extract sample names, save as list (one sample per line) to a text
# file called samples.grp
# head and tail to select third line, awk with a for loop to print the 
# fields greater than 3, each on a separate line.
head -n 3 $1 | tail -n 1 | awk 'BEGIN { FS = "\t" } ; { for ( i = 3 ; i <= NF ; i++ ) print $i }' > samples.grp
# alternative solution using tr
# head -n 3 $1| tail -n 1 | tr "\t" "\n" | tail -n +3 > samples.grp

# q4.2: rename samples to a fixed width format with zero padded digits.
# sort alphabetically and save as sorted_samples.grp
# awk to print first letter, then the number with a trailing zero format, follwed by a newline
# then sort and direct to output file 
awk 'BEGIN { FS="" } ; {printf $1} ; {printf "%02d\n", int($2$3) }' samples.grp | sort  > sorted_samples.grp
