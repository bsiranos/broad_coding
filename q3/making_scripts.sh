sed 's/1_1000/1001_2000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 1001 2000/g' > regression_1001_2000.sh
sed 's/1_1000/2001_3000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 2001 3000/g' > regression_2001_3000.sh
sed 's/1_1000/3001_4000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 3001 4000/g' > regression_3001_4000.sh
sed 's/1_1000/4001_5000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 4001 5000/g' > regression_4001_5000.sh
sed 's/1_1000/5001_6000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 5001 6000/g' > regression_5001_6000.sh
sed 's/1_1000/6001_7000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 6001 7000/g' > regression_6001_7000.sh
sed 's/1_1000/7001_8000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 7001 8000/g' > regression_7001_8000.sh
sed 's/1_1000/8001_9000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 8001 9000/g' > regression_8001_9000.sh
sed 's/1_1000/9001_10000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 9001 10000/g' > regression_9001_10000.sh
sed 's/1_1000/10001_11000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 10001 11000/g' > regression_10001_11000.sh
sed 's/1_1000/11001_12000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 11001 12000/g' > regression_11001_12000.sh
sed 's/1_1000/12001_13000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 12001 13000/g' > regression_12001_13000.sh
sed 's/1_1000/13001_14000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 13001 14000/g' > regression_13001_14000.sh
sed 's/1_1000/14001_15000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 14001 15000/g' > regression_14001_15000.sh
sed 's/1_1000/15001_16000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 15001 16000/g' > regression_15001_16000.sh
sed 's/1_1000/16001_17000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 16001 17000/g' > regression_16001_17000.sh
sed 's/1_1000/17001_18000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 17001 18000/g' > regression_17001_18000.sh
sed 's/1_1000/18001_19000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 18001 19000/g' > regression_18001_19000.sh
sed 's/1_1000/19001_20000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 19001 20000/g' > regression_19001_20000.sh
sed 's/1_1000/20001_21000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 20001 21000/g' > regression_20001_21000.sh
sed 's/1_1000/21001_22000/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 21001 22000/g' > regression_21001_22000.sh
sed 's/1_1000/22001_22268/g' regression_1_1000.sh | sed 's/sh 1 1000/sh 22001 22268/g' > regression_22001_22268.sh

for i in $(ls *.csv); do sed -i -e 's/\"//g' $i ; done
for i in $(ls *.csv); do sort -n $i > $i.2; mv $i.2 $i;	 done 

for i in $(ls r2*); do cat $i >> r2_all.csv ; done
sort -n r2_all.csv > r2_all.csv.sorted ; mv r2_all.csv.sorted r2_all.csv 

for i in $(ls predictions*); do cat $i >> predictions_all.csv ; done
sort -n predictions_all.csv > predictions_all.csv.sorted ; mv predictions_all.csv.sorted predictions_all.csv 

for i in $(ls residuals*); do cat $i >> residuals_all.csv ; done
sort -n residuals_all.csv > residuals_all.csv.sorted ; mv residuals_all.csv.sorted residuals_all.csv 

mkdir log2
mkdir log2/individual 
mv *_all.csv log2
mv *.csv log2/individual

resid <- as.matrix(read.table("residuals_all.csv",sep=',', row.names = 1))
r2vals <- t(as.matrix(read.table("r2_all.csv",sep=',', row.names = 1)))

rmse <- apply(resid,1, function(x) sqrt(mean(x^2)))

pdf("regression_rmse_log.pdf")
hist(rmse, breaks=30, col='blue', main='Histogram of RMSE values', xlab="RMSE")
dev.off()

pdf("regression_r2_log.pdf")
hist(r2vals, breaks=30, col='blue', main='Histogram of r^2 values', xlab="r^2")
dev.off()


# make the gct file from the predictions_all.csv file 
# sort the csv file
sort -s -n -k 1,1 -t,  predictions_all.csv > predictions_all_sorted.csv
# convert csv to tab
cat predictions_all_sorted.csv | tr , '\t' > predictions_all.gct


echo "#1.2" > predicted.gct
echo "22268,96" >> predicted.gct
head -n 3 test.gct | tail -n 1 >> predicted.gct

# get row names from the testing dataset
paste <(tail -n +4 train.gct | cut -f 1,2) <(cut -f 2- predictions_all.gct) >> predicted.gct
