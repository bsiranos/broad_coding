# script to run the regression algorithm on a specified subset of the data
# this script was used to call the Rscript on a computing cluster and has 
# a path hardcoded into it. Not very relevant beyond this assignment,
# but I'm including it anyway.
cd /scratch/bsiranos/broad_coding_data/data/q3
Rscript ~/projects/broad_coding/q3/regression_multi.R $1 $2
