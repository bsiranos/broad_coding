# these commands are used to clean up the outputs of regression_multi.R
# after I split up the job to multiple processors. You must be in a 
# directory with the output files and the original training/testing
# data for these commands to work. 

# remove quotes - should have done this in the Rscript!
for i in $(ls *.csv); do sed -i -e 's/\"//g' $i ; done

# combine all csv files from multiple runs into one
for i in $(ls r2*); do cat $i >> r2_all.csv ; done
for i in $(ls predictions*); do cat $i >> predictions_all.csv ; done
for i in $(ls residuals*); do cat $i >> residuals_all.csv ; done

# make the gct file from the predictions_all.csv file 
# sort the csv file
sort -s -n -k 1,1 -t,  predictions_all.csv > predictions_all_sorted.csv
# convert csv to tab
cat predictions_all_sorted.csv | tr , '\t' > predictions_all.gct

# make the predicted.gct file. Get the header information
echo "#1.2" > predicted.gct
# Ideally I could scan a file to get these numbers, but I'm sure of them here
echo "22268,96" >> predicted.gct
# column names from test.gct
head -n 3 test.gct | tail -n 1 >> predicted.gct

# get row names from the training dataset
paste <(tail -n +4 train.gct | cut -f 1,2) <(cut -f 2- predictions_all.gct) >> predicted.gct


