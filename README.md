# Broad Institute ATP Coding Assessment
###### Ben Siranosian  
### Results and description
results.pdf describes the analysis I conducted and my interpretations of the data.
### Source code and output files
Source code and output files can be found in a folder corresponding to the question number. 
### Questions?
Contact benjamin_siranosian@brown.edu